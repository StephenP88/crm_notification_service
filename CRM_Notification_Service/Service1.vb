﻿Imports System.Net
Imports System.Net.Mail
Imports System.Threading
Imports CRM_Notification_Service.Utilities
Imports System.Timers



Public Class Service1
    Public gConnStringLeads As String = "host= leads.vmgdms.com;database=leads;username =leads_user ;password =Ai6&DUpTxcy=muQkxvqb; port=45206; Pooling=false;CommandTimeout=150;Timeout=150"
    Private trd As Thread
    Public myTimer As New Timers.Timer
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        AutoLog = False
        If Not System.Diagnostics.EventLog.SourceExists("CRM_Notification") Then
            System.Diagnostics.EventLog.CreateEventSource("CRM_Notification", "Event Log")
        End If

        EventLog1.Source = "CRM_Notification"

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        EventLog1.WriteEntry("CRM Notification started")
        myTimer.Interval = 180000
        'AddHandler myTimer.Elapsed, AddressOf send_notification
        AddHandler myTimer.Elapsed, AddressOf Create_Thread
        myTimer.Start()
        myTimer.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        EventLog1.WriteEntry("CRM Notification stopped")
        If IsNothing(trd) = False And trd.IsAlive Then
            trd.Interrupt()
            trd.Abort()
        End If
        myTimer.Stop()
        myTimer.Enabled = False
        Dim mail As New MailMessage
        mail.To.Add("stephen@vmgsoftware.co.za")
        mail.CC.Clear()
        mail.CC.Add("justin@vmgsoftware.co.za")
        Mail.Subject = "CRM Notification Error"
        Mail.IsBodyHtml = True

        Mail.Body = "CRM Notification Service has stopped"

        EmailService.SendMail(Mail)
    End Sub
    Sub Create_Thread()
        If IsNothing(trd) OrElse Not trd.IsAlive Then
            trd = New Thread(AddressOf send_notification)
            trd.Priority = ThreadPriority.Normal
            trd.IsBackground = True
            trd.Start()
        End If
    End Sub


    Sub send_notification()
        Dim lLead_id As Double = 0
        Dim lDealer_id As Double = 0
        Dim lsms As String = ""
        Dim lselling As String = ""
        Dim tracking_id As String = ""
        Dim lCellPhone As String = ""
        Dim smsStatus As String = ""
        Dim lInsertstr As String = ""
        Dim max_no As Double = 0
        Dim current As Double = 1
        Dim active As String

        Do

            Try
                If DnsInternetConnection() Then
                    EventLog1.WriteEntry("Checking for new leads", EventLogEntryType.Information)
                    Dim remoteDb = New NpgDatabaseHelper(gConnStringLeads)
                    Dim remoteData As DataTable = remoteDb.ExecuteDataSet($"select lead_id, lead_name, cellphone_no, lead_source, make, model_desc, model, selling, dealer_id,  " &
                                                             "contact_person, contact_no, active, price from   leads_v1.sms_to_send ", "online").Tables("online")

                    If remoteData.Rows.Count > 0 Then
                        EventLog1.WriteEntry(remoteData.Rows.Count.ToString & " new leads found.", EventLogEntryType.Information)
                        max_no = remoteData.Rows.Count
                        For Each dataRow As DataRow In remoteData.Rows
                            active = dataRow.Item("active").ToString
                            current = current + 1
                            lLead_id = CDbl(dataRow.Item("lead_id").ToString)
                            lDealer_id = CDbl(dataRow.Item("dealer_id").ToString)

                            If active = "True" Then
                                lselling = dataRow.Item("selling").ToString
                                If lselling = "" OrElse lselling = "False" Then
                                    lsms = dataRow.Item("lead_name").ToString & ", " & dataRow.Item("cellphone_no").ToString & ", " & dataRow.Item("lead_source").ToString & ", " & dataRow.Item("make").ToString & " " & dataRow.Item("model").ToString & ", " & FormatCurrency(CDbl(dataRow.Item("price").ToString))
                                Else
                                    lsms = dataRow.Item("lead_name").ToString & ", " & dataRow.Item("cellphone_no").ToString & ", " & dataRow.Item("lead_source").ToString & ", Selling " & dataRow.Item("make").ToString & " " & dataRow.Item("model").ToString & ", " & FormatCurrency(CDbl(dataRow.Item("price").ToString))
                                End If

                                lCellPhone = dataRow.Item("contact_no").ToString
                                If lCellPhone.StartsWith("0") Then
                                    lCellPhone = "27" & lCellPhone.Remove(0, 1)
                                End If

                                tracking_id = SendSMS(lCellPhone, lsms)
                                smsStatus = GetCRMSmsStatus(tracking_id)

                                lInsertstr = "INSERT INTO leads_v1.sms_sent( lead_id, dealer_id, message_sent, tracking_id, status_code) " &
                                             "VALUES(" & lLead_id & ", " & lDealer_id & ",'" & lsms & "' , '" & tracking_id & "',  '" & smsStatus & "')"
                                remoteDb.ExecuteNonQuery(lInsertstr)
                            Else
                                lInsertstr = "INSERT INTO leads_v1.sms_sent( lead_id, dealer_id, message_sent, tracking_id, status_code) " &
                                            "VALUES(" & lLead_id & ", " & lDealer_id & ",'No Active contact' , '',  '9999')"
                                remoteDb.ExecuteNonQuery(lInsertstr)
                            End If
                        Next
                    Else
                        EventLog1.WriteEntry("No new leads", EventLogEntryType.Information)
                        GoTo exit_loop
                    End If
                    remoteDb.CloseConnection()
                    Thread.Sleep(100)
                    GoTo exit_loop
                Else
                    EventLog1.WriteEntry("No Internet", EventLogEntryType.Warning)
                    GoTo exit_loop
                End If
            Catch ex As Exception
                EventLog1.WriteEntry(ex.Message, EventLogEntryType.Error)
                Dim stkTrace As String = ex.StackTrace
                Dim excMessage As String = ex.Message
                Dim mail As New MailMessage
                mail.To.Add("stephen@vmgsoftware.co.za")
                mail.CC.Clear()
                mail.CC.Add("justin@vmgsoftware.co.za")
                mail.Subject = "CRM Notification Error"
                mail.IsBodyHtml = True

                mail.Body = "<h3>Error on the CRM Notification system</h3><table><tbody><tr><td>Title : </td><td>CRM Notification Error</td></tr>" &
                            "<tr><td>Message : </td><td>send_notification</td></tr><tr><td>System Message:</td><td>" & excMessage & "</td></tr>" &
                            "<tr><td>Stack Trace:</td><td>" & stkTrace & "</td></tr><tr><td>Date/Time:</td><td>" & DateTime.Now.ToString() & "</td></tr></tbody></table>"

                EmailService.SendMail(mail)
                GoTo exit_loop
            End Try
        Loop
exit_loop:
    End Sub

    Public Function DnsInternetConnection() As Boolean
        Try
            Dim ipHe As IPHostEntry = Dns.GetHostEntry("www.google.com")
            Return True
        Catch
            Return False
        End Try
    End Function

    Function SendSMS(CellNumber As String, smsText As String) As String
        Try
            Dim webclient As WebClient = New WebClient()
            Dim result As String =
                    webclient.DownloadString(
                        "http://api.clickatell.com/http/sendmsg?user=vmgza&password=DancingMonkey7802&api_id=3291264&to=" &
                        CStr(CellNumber) & "&text=" & smsText)
            SendSMS = result
        Catch ex As Exception
            SendSMS = "Error"
            EventLog1.WriteEntry(ex.Message, EventLogEntryType.Error)
            Dim stkTrace As String = ex.StackTrace
            Dim excMessage As String = ex.Message
            Dim mail As New MailMessage
            mail.To.Add("stephen@vmgsoftware.co.za")
            mail.CC.Clear()
            mail.CC.Add("justin@vmgsoftware.co.za")
            mail.Subject = "CRM Notification Error"
            mail.IsBodyHtml = True

            mail.Body = "<h3>Error on the CRM Notification system</h3><table><tbody><tr><td>Title : </td><td>CRM Notification Error</td></tr>" &
                        "<tr><td>Message : </td><td>send_notification</td></tr><tr><td>System Message:</td><td>" & excMessage & "</td></tr>" &
                        "<tr><td>Stack Trace:</td><td>" & stkTrace & "</td></tr><tr><td>Date/Time:</td><td>" & DateTime.Now.ToString() & "</td></tr></tbody></table>"

            EmailService.SendMail(mail)
        End Try

    End Function

    Public Function GetCRMSmsStatus(SMSID As String) As String
        'Run through all the local sms registrations that have no sms status and check and update what the current status is of that sms
        Dim returnCode As String = ""
        Try
            Dim webClient As WebClient = New WebClient()

            Dim result As String =
                        webClient.DownloadString(
                            "http://api.clickatell.com/http/querymsg?user=vmgza&password=DancingMonkey7802&api_id=3291264&apimsgid=" &
                            Replace(SMSID, "ID: ", ""))
            GetCRMSmsStatus = (result.Substring(result.IndexOf("Status: ", StringComparison.Ordinal) + 8, 3))

        Catch ex As Exception
            GetCRMSmsStatus = "001"
            EventLog1.WriteEntry(ex.Message, EventLogEntryType.Error)
            Dim stkTrace As String = ex.StackTrace
            Dim excMessage As String = ex.Message
            Dim mail As New MailMessage
            mail.To.Add("stephen@vmgsoftware.co.za")
            mail.CC.Clear()
            mail.CC.Add("justin@vmgsoftware.co.za")
            mail.Subject = "CRM Notification Error"
            mail.IsBodyHtml = True

            mail.Body = "<h3>Error on the CRM Notification system</h3><table><tbody><tr><td>Title : </td><td>CRM Notification Error</td></tr>" &
                            "<tr><td>Message : </td><td>send_notification</td></tr><tr><td>System Message:</td><td>" & excMessage & "</td></tr>" &
                            "<tr><td>Stack Trace:</td><td>" & stkTrace & "</td></tr><tr><td>Date/Time:</td><td>" & DateTime.Now.ToString() & "</td></tr></tbody></table>"

            EmailService.SendMail(mail)
            Exit Function
        Finally

        End Try

    End Function
End Class
